
<?php
	require "AltoRouter.php";

	// MYSQL CONNECT
		function connect(){
			$con = mysqli_connect("45.32.62.40", "root", "asugfr", "asugfr");
			if (!$con) {
				header("HTTP/1.1 500 Not Found");
				die("Could not connect to DB");
			}
			mysqli_set_charset($con, "utf8");

			return $con;
		}
	// --------------------------------------------------------------

	// ALTOROUTER INSTANCE
		$router = new AltoRouter();
		$router->setBasePath("/others_asugfr/asugfr_api/index.php");
		// $router->setBasePath("/narra/others_asugfr/asugfr_api/index.php");

		$router->map("GET", "/", function() {
			connect();
			header("Content-Type: application/json");
			echo json_encode(array("txt" => "hello world!"));
		});
	// --------------------------------------------------------------


	// login
	$router->map("GET", "/login/[a:username]/[a:password]", function($username, $password) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT * FROM teacher WHERE BINARY(teacher_uname)=BINARY('".$username."') and BINARY(teacher_pword)=BINARY('".$password."') LIMIT 1;")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// get subjects by teacher
	// get section by teacher by subject
	$router->map("GET", "/classes/[:t_fname]", function($t_fname) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT * FROM teacher_assign WHERE BINARY t_fname='".urldecode($t_fname)."';")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// get enrolled students per teacher - subject - section
	$router->map("GET", "/enrollees/[:t_fname]/[:t_subject]/[:t_section]", function($tfname, $tsubject, $tsection) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT a.*, b.stud_sex FROM student_assign a, student b WHERE BINARY a.s_name=b.stud_fname AND a.s_teacher='".urldecode($tfname)."' AND a.s_subject='".urldecode($tsubject)."' AND a.s_section='".urldecode($tsection)."' ORDER BY a.s_name ASC;")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// create quiz by teacher - subject - section
	$router->map("GET", "/new/quiz/[:t_id]/[:q_name]", function($t_id, $q_name) {
		$con = connect();
		if ($query = mysqli_query($con, "INSERT INTO quiz(q_name, t_id) VALUES ('".urldecode($q_name)."', ".urldecode($t_id).");")){
			header("Content-Type: application/json");
			echo json_encode(array("affected"=>mysqli_affected_rows($con)));
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// get all quizzes by teacher
	$router->map("GET", "/quizzes/[:t_id]", function($t_id) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT * FROM quiz WHERE BINARY t_id='".urldecode($t_id)."';")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// record quiz by teacher - subject - section - quiz
	$router->map("GET", "/record/quiz/[:q_id]/[:s_id]/[:score]", function($q_id, $s_id, $score) {
		$con = connect();
		if ($query = mysqli_query($con, "INSERT INTO quiz_score(q_id, s_id, score) VALUES (".urldecode($q_id).", ".urldecode($s_id).", ".urldecode($score).") ON DUPLICATE KEY UPDATE score=".urldecode($score).";")){
			header("Content-Type: application/json");
			echo json_encode(array("affected"=>mysqli_affected_rows($con)));
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// record attendance for today
	$router->map("GET", "/record/attendance/[:att_date]/[:s_id]/[:att_present]", function($att_date, $s_id, $att_present) {
		$con = connect();
		$att_date = str_replace("-", "/", $att_date);
		if ($query = mysqli_query($con, "INSERT INTO attendance_sheet(att_date, s_id, att_present) VALUES ('".urldecode($att_date)."', ".urldecode($s_id).", ".urldecode($att_present).") ON DUPLICATE KEY UPDATE att_present=".urldecode($att_present).";")){
			header("Content-Type: application/json");
			echo json_encode(array("affected"=>mysqli_affected_rows($con)));
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// record face ID
	$router->map("GET", "/record/face/[:s_id]/[:s_img]", function($s_id, $s_img) {
		$con = connect();
		if ($query = mysqli_query($con, "UPDATE student_assign SET s_img='".urldecode($s_img)."' WHERE BINARY s_id=".urldecode($s_id)." LIMIT 1;")){
			header("Content-Type: application/json");
			echo json_encode(array("affected"=>mysqli_affected_rows($con)));
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// get all scores
	$router->map("GET", "/quiz/scores/[:q_id]", function($q_id) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT * FROM quiz_score WHERE BINARY q_id='".urldecode($q_id)."';")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// get all attendance
	$router->map("GET", "/attendance/[:att_date]/[:s_ids]", function($att_date, $s_ids) {
		$con = connect();
		if ($query = mysqli_query($con, "SELECT * FROM attendance_sheet WHERE BINARY att_date='".urldecode($att_date)."' AND s_id in (".urldecode($s_ids).");")){
			$result = array();
			while ($row = $query->fetch_assoc()) { array_push($result, $row); }

			header("Content-Type: application/json");
			echo json_encode($result);
		}
		else{ header("HTTP/1.1 500 Internal Server Error"); }
	});


	// match current request url
	$match = $router->match();

	// call closure or throw 404 status
	if( $match && is_callable( $match["target"] ) ) { call_user_func_array($match["target"], $match["params"]); }
	else { header("HTTP/1.1 404 Not Found");  }
?>