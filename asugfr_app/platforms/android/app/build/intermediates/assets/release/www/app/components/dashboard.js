

Vue.component("dashboard", {
	props: ["settings"],
	template: "#dashboard",
	data: function(){
		return {
			subject: "",
			section: "",
			classes: [],
		}
	},
	computed: {
		unique_sections: function(){
			var sections = [];
			for (var i = 0; i < this.classes.length; i++) {
				if(this.classes[i]["t_subject"] == this.subject){ sections.push(this.classes[i]["t_section"]); }
			}
			return sections;
		},
		unique_subjects: function(){
			var classNames = [];
			var classes = [];
			for (var i = 0; i < this.classes.length; i++) {
				if(classNames.indexOf(this.classes[i]["t_subject"]) < 0){ 
					classes.push(this.classes[i]["t_subject"]);
					classNames.push(this.classes[i]["t_subject"]);
				}
			}
			return classes;
		},
		classSelected: function(){
			for (var i = 0; i < this.classes.length; i++) {
				if(this.classes[i]["t_subject"] == this.subject && this.classes[i]["t_section"] == this.section){ return this.classes[i] }
			}
		},
	},
	methods:{
		getClasses: function(t_fname){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/classes/" + t_fname), "", function(response){
				thisApp.classes = response;
			});
		},
		loadSheet: function(sheet){
			// trigger updateSettings - event in parent component
			this.settings["renderPage"] = sheet;
			this.settings["sheet-data"] = this.classSelected;
			this.$parent.$emit("updateSettings", this.settings);
		},
	},
	mounted: function(){
		console.log("<dashboard>: Init!");

		this.getClasses(this.settings["teacher"]["teacher_fname"]);
	},
	watch: {}
});