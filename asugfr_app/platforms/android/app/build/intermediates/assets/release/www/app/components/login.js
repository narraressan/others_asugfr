

Vue.component("login", {
	props: ["settings"],
	template: "#login",
	data: function(){
		return {
			login_username: "",
			login_password: "",
		}
	},
	computed: {},
	methods:{
		loginAccount: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/login/" + this.login_username + "/" + this.login_password), "", function(response){
				if(response.length > 0){
					// trigger updateSettings - event in parent component
					thisApp.settings["teacher"] = response[0];
					thisApp.settings["renderPage"] = "dashboard";
					thisApp.$parent.$emit("updateSettings", thisApp.settings);
				}
				else{ thisApp.notify("Unknown credentials"); }
			});
		},
	},
	mounted: function(){
		console.log("<login>: Init!");
	},
	watch: {}
});