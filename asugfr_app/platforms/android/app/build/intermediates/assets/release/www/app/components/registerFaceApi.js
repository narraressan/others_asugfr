

Vue.component("register-face-api", {
	props: ["settings"],
	mixins: [msCognitiveFaceAPI],
	template: "#register-face-api",
	data: function(){
		return {
			imageURI: "",
			faceId: "",
			personId: "",
		}
	},
	computed: {},
	methods:{
		registerFaceID: function(evt){
			NProgress.start();
			var subscriptionKey = "fd8ee8107c4e4301b35a953e4c175b93";
			var personId = (Math.floor(Math.random() * 10000000) + 1);
			var thisApp = this;

			var imageFile = evt.target.files[0];
			this.createImage(imageFile);
			var reader = new FileReader();
			var fileType;

			//wire up the listener for the async 'loadend' event
			reader.addEventListener('loadend', function () {
				var fileContentArrayBuffer = reader.result;

				// CREATE NEW PERSON! - as random key
				$.ajax({
					url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/asugfr/persons?",
					beforeSend: function (xhrObj){ 
						xhrObj.setRequestHeader("Content-Type", "application/json"); 
						xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
					},
					type: "POST",
					data: JSON.stringify({ "name": personId, "userData": JSON.stringify(thisApp.settings["student-data"]) })
				})
				.fail(function (err) { thisApp.notify("Oops! execution failed - New Person"); NProgress.done(); })
				.done(function (data) {
					// ADD FACE TO THIS PERSON
					thisApp.personId = data["personId"];
					$.ajax({
						url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/asugfr/persons/" + thisApp.personId + "/persistedFaces",
						beforeSend: function (xhrObj){ xhrObj.setRequestHeader("Content-Type", "application/octet-stream"); xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
						},
						type: "POST",
						processData: false,
						data: new Blob([fileContentArrayBuffer], { type: fileType })
					})
					.fail(function (err) { thisApp.notify("Oops! execution failed - Adding Face"); NProgress.done(); })
					.done(function (data) {
						thisApp.notify("Face ID generated");
						thisApp.faceId = data["persistedFaceId"];
						thisApp.saveFaceID(thisApp.personId);
						NProgress.done();
					});
				});
			});

			if (imageFile) {
				fileType = imageFile.type;
				reader.readAsArrayBuffer(imageFile);
			}
		},
		backtoSheets: function(){
			this.settings["renderPage"] = "attendance-sheets";
			this.$parent.$emit("updateSettings", this.settings);
		},
		takePic: function(){ 
			var settings = {
				quality: 50,
				destinationType: Camera.DestinationType.FILE_URI,
				correctOrientation: true,
				saveToPhotoAlbum: true
			}
			navigator.camera.getPicture(this.onSuccess, this.onFail, settings); 
		},
		onSuccess: function(imageData){ this.imageURI = imageData; },
		onFail: function(message){ console.log(message); },
		createImage(file) {
			var image = new Image();
			var reader = new FileReader();
			var thisApp = this;

			reader.onload = (e) => { thisApp.imageURI = e.target.result; };
			reader.readAsDataURL(file);
		},
		saveFaceID: function(faceId){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/record/face/" + this.settings["student-data"]["s_id"] + "/" + faceId), "", function(response){
				console.log(response);
			});
		}
	},
	mounted: function(){
		console.log("<register-face-api>: Init!");
	},
	watch: {}
});