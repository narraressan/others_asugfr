

Vue.component("take-attendance-pic", {
	props: ["settings"],
	mixins: [msCognitiveFaceAPI],
	template: "#take-attendance-pic",
	data: function(){
		return {
			imageURI: "",
			faceCount: "",
		}
	},
	computed: {},
	methods:{
		flagPresent: function(faceIds){
			var finDate = this.settings["attendance-date"];
			for (var i = 0; i < faceIds.length; i++) {
				for (var j = 0; j < faceIds[i]["candidates"].length; j++) {
					// consider only those with high confidence
					if(faceIds[i]["candidates"][j]["confidence"] > 0.5){
						if(this.settings["attendance-enrollees"][faceIds[i]["candidates"][j]["personId"]]){
							var s_id = this.settings["attendance-enrollees"][faceIds[i]["candidates"][j]["personId"]];
							this.request("GET", (this.settings["host"] + "/record/attendance/" + finDate + "/" + s_id + "/1"), "", function(response){
								console.log(response);
							});
						}
					}
				}
			}
			this.notify(faceIds.length + " faces detected");
		},
		detectFaceIDs: function(evt){
			NProgress.start();
			var thisApp = this;
			var subscriptionKey = "fd8ee8107c4e4301b35a953e4c175b93"
			var imageFile = evt.target.files[0];
			this.createImage(imageFile);
			var reader = new FileReader();
			var fileType;

			//wire up the listener for the async 'loadend' event
			reader.addEventListener('loadend', function () {
				var fileContentArrayBuffer = reader.result;

				// RUN FACE DETECT AND GET IDs
				var params = { "returnFaceId": "true" };
				$.ajax({
					url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect?" + $.param(params),
					beforeSend: function (xhrObj){ 
						xhrObj.setRequestHeader("Content-Type", "application/octet-stream");
						xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
					},
					type: "POST",
					processData: false,
					data: new Blob([fileContentArrayBuffer], { type: fileType })
				})
				.fail(function (err) { thisApp.notify("Oops! execution failed - Face ID"); NProgress.done(); })
				.done(function (data) {
					if(data.length > 0){ 
						var faceIds_dected = []
						for (var i = 0; i < data.length; i++) { faceIds_dected.push(data[i]["faceId"]); }

						$.ajax({
							url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/identify",
							beforeSend: function (xhrObj) {
								xhrObj.setRequestHeader("Content-Type", "application/json");
								xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
							},
							type: "POST",
							data: JSON.stringify({ "personGroupId": "asugfr", "faceIds": faceIds_dected, "maxNumOfCandidatesReturned": 5 })
						})
						.fail(function (err) { thisApp.notify("Oops! execution failed"); NProgress.done(); })
						.done(function (data) {
							// console.log(data);
							thisApp.flagPresent(data);
							NProgress.done();
						});
					}
					else{ 
						thisApp.notify("No Face IDs detected");
						NProgress.done();
					}
				});
			});

			if (imageFile) {
				fileType = imageFile.type;
				reader.readAsArrayBuffer(imageFile);
			}
		},
		backtoSheets: function(){
			this.settings["renderPage"] = "attendance-sheets";
			this.$parent.$emit("updateSettings", this.settings);
		},
		createImage(file) {
			var image = new Image();
			var reader = new FileReader();
			var thisApp = this;

			reader.onload = (e) => { thisApp.imageURI = e.target.result; };
			reader.readAsDataURL(file);
		},
		takePic: function(){ 
			var settings = {
				quality: 50,
				destinationType: Camera.DestinationType.FILE_URI,
				correctOrientation: true,
				saveToPhotoAlbum: true
			}
			navigator.camera.getPicture(this.onSuccess, this.onFail, settings); 
		},
		onSuccess: function(imageData){ this.imageURI = imageData; },
		onFail: function(message){ console.log(message); },
	},
	mounted: function(){
		console.log("<take-attendance-pic>: Init!");
	},
	watch: {}
});