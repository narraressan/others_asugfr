


// global functions
Vue.mixin({
	methods: {
		notify: function(message){ notie.alert({ text: message, position: "bottom" }); },
		request: function(thisMethod, thisURL, thisData, success){
			var thisVue = this;
			NProgress.start();
			$.ajax({
				method: thisMethod,
				url: thisURL,
				contentType: "application/json",
				data: JSON.stringify(thisData)
			})
			.fail(function(response){
				thisVue.notify("Oops! execution failed");
				NProgress.done(); 
			})
			.done(function(data, textStatus, xhr){
				success(data);
				NProgress.done();
			});
		},
	}
})