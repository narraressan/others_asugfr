

Vue.component("quiz-sheets", {
	props: ["settings"],
	template: "#quiz-sheets",
	data: function(){
		return {
			quizzes: [],
			quiz: "",
			newQuiz: "",
			enrollees: [],
			scores: {},
		}
	},
	computed: {},
	methods:{
		createQuiz: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/new/quiz/" + this.settings["sheet-data"]["t_id"] + "/" + this.newQuiz), "", function(response){
				thisApp.notify("Quiz has been created");
				thisApp.newQuiz = "";
				thisApp.getQuizzes();
			});
		},
		getQuizzes: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/quizzes/" + this.settings["sheet-data"]["t_id"]), "", function(response){
				thisApp.quizzes = response;
			});
		},
		getEnrollees: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/enrollees/" + this.settings["teacher"]["teacher_fname"] + "/" + this.settings["sheet-data"]["t_subject"] + "/" + this.settings["sheet-data"]["t_section"]), "", function(response){
				thisApp.enrollees = response;
			});
		},
		saveScore: function(nth, el){
			var val = $(el.target).val();
			if(val.trim() != ""){
				var thisApp = this;
				this.request("GET", (this.settings["host"] + "/record/quiz/" + this.quiz["q_id"] + "/" + nth["s_id"] + "/" + parseInt(val)), "", function(response){
					thisApp.notify("score has been saved");
					$(el.target).val("");
					thisApp.getScores();
				});
			}
		},
		getScores: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/quiz/scores/" + this.quiz["q_id"]), "", function(response){
				thisApp.scores = {};
				if(response.length > 0){
					for (var i = 0; i < response.length; i++) { thisApp.scores[response[i]["s_id"]] = response[i]; }
				}
			});
		},
	},
	mounted: function(){
		console.log("<quiz-sheets>: Init!");

		this.getQuizzes();
		this.getEnrollees();
	},
	watch: {
		quiz: function(){ this.getScores(); }
	}
});