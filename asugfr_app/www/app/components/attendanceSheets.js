

Vue.component("attendance-sheets", {
	props: ["settings"],
	template: "#attendance-sheets",
	data: function(){
		return {
			date: (new Date().toISOString().substring(0, 10)),
			enrollees: [],
			attendance: [],
			subsKey: this.subsKey_general(),
		}
	},
	computed: {},
	methods:{
		registerNewFaceID: function(nth){
			this.settings["renderPage"] = "register-face-api";
			this.settings["student-data"] = nth;
			this.$parent.$emit("updateSettings", this.settings);
		},
		takeAttendance: function(sheet){
			this.settings["renderPage"] = "take-attendance-pic";
			this.settings["attendance-date"] = this.date.replace(/\//g, "-");
			this.settings["attendance-enrollees"] = {};

			for (var i = 0; i < this.enrollees.length; i++) {
				if(this.enrollees[i]["s_img"] != ""){ this.settings["attendance-enrollees"][this.enrollees[i]["s_img"]] = this.enrollees[i]["s_id"]; }
			}

			this.$parent.$emit("updateSettings", this.settings);
		},
		getEnrollees: function(){
			var thisApp = this;
			this.request("GET", (this.settings["host"] + "/enrollees/" + this.settings["teacher"]["teacher_fname"] + "/" + this.settings["sheet-data"]["t_subject"] + "/" + this.settings["sheet-data"]["t_section"]), "", function(response){
				thisApp.enrollees = response;
				thisApp.getAttendance();
			});
		},
		thumbnail: function(gender){
			if(gender == "Female"){ return "female.png"; }
			else{ return "male.png" }
		},
		flagPresent: function(nth, event){
			var finDate = this.date.replace(/\//g, "-");
			this.request("GET", (this.settings["host"] + "/record/attendance/" + finDate + "/" + nth["s_id"] + "/" + event.target.checked), "", function(response){
				console.log(response);
			});
		},
		getAttendance: function(){
			var ids = "";
			for (var i = 0; i < this.enrollees.length; i++) {
				if(this.enrollees[i]["s_id"] != null && this.enrollees[i]["s_id"] !== undefined){
					ids += this.enrollees[i]["s_id"] + "";
					if(i != (this.enrollees.length-1)){ ids += ","; }
				}
			}

			var thisApp = this;
			var finDate = this.date.replace(/\//g, "-");
			this.request("GET", (this.settings["host"] + "/attendance/" + finDate + "/" + ids), "", function(response){
				// console.log(response);
				thisApp.attendance = response;
			});
		},
		checkAttendance: function(nth){
			for (var i = 0; i < this.attendance.length; i++) {
				if(this.attendance[i]["s_id"] == nth["s_id"]){ return parseInt(this.attendance[i]["att_present"]); }
			}
		},
		trainGroup: function(){
			NProgress.start();
			var thisApp = this;
			$.ajax({
				url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/persongroups/asugfr/train",
				beforeSend: function (xhrObj) {
					xhrObj.setRequestHeader("Content-Type","application/json");
					xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", thisApp.subsKey);
				},
				type: "POST",
			})
			.fail(function (err) { thisApp.notify("Oops! execution failed"); NProgress.done(); })
			.done(function (data) {
				thisApp.notify("AI is training");
				NProgress.done();
			});
		},
	},
	mounted: function(){
		console.log("<attendance-sheets>: Init!");

		this.getEnrollees();
	},
	watch: {}
});