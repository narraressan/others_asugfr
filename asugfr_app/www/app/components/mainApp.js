

Vue.component("main-app", {
	props: ["settings"],
	template: "#main-app",
	data: function(){
		return {}
	},
	computed: {},
	methods:{
		dashboard: function(){
			this.settings["renderPage"] = "dashboard";
			this.$parent.$emit("updateRenderPage", this.settings); 
		}
	},
	mounted: function(){
		console.log("<main-app>: Init!");

		this.$on("updateSettings", function(newSettings){ 
			// trigger the updateSettings - event in vueApp
			this.$parent.$emit("updateRenderPage", newSettings); 
		});
	},
	watch: {}
});