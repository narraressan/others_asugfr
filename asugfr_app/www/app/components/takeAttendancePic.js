

Vue.component("take-attendance-pic", {
	props: ["settings"],
	mixins: [msCognitiveFaceAPI],
	template: "#take-attendance-pic",
	data: function(){
		return {
			imageURI: "",
			faceCount: "",
			subsKey: this.subsKey_general(),
		}
	},
	computed: {},
	methods:{
		flagPresent: function(faceIds){
			var finDate = this.settings["attendance-date"];
			for (var i = 0; i < faceIds.length; i++) {
				for (var j = 0; j < faceIds[i]["candidates"].length; j++) {
					// consider only those with high confidence
					if(faceIds[i]["candidates"][j]["confidence"] > 0.5){
						if(this.settings["attendance-enrollees"][faceIds[i]["candidates"][j]["personId"]]){
							var s_id = this.settings["attendance-enrollees"][faceIds[i]["candidates"][j]["personId"]];
							this.request("GET", (this.settings["host"] + "/record/attendance/" + finDate + "/" + s_id + "/1"), "", function(response){
								console.log(response);
							});
						}
					}
				}
			}
			this.notify(faceIds.length + " faces detected");
		},
		backtoSheets: function(){
			this.settings["renderPage"] = "attendance-sheets";
			this.$parent.$emit("updateSettings", this.settings);
		},
		takePic: function(){
			var settings = {
				quality: 50,
				destinationType: Camera.DestinationType.FILE_URI,
				correctOrientation: true,
				saveToPhotoAlbum: true
			}
			navigator.camera.getPicture(this.onSuccess, this.onFail, settings);
		},
		onSuccess: function(imageData){
			this.imageURI = imageData;

			NProgress.start();
			var thisApp = this;
			var subscriptionKey = this.subsKey;
			var fileType;

			var request = new XMLHttpRequest();
			request.open("GET", this.imageURI, true);
			request.responseType = "blob";
			request.onload = function() {
				var reader = new FileReader();
				fileType = request.response.type;
				reader.readAsArrayBuffer(request.response);

				reader.onload = function(e){
					var fileContentArrayBuffer = e.target.result;

					// RUN FACE DETECT AND GET IDs
					var params = { "returnFaceId": "true" };
					$.ajax({
						url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect?" + $.param(params),
						beforeSend: function (xhrObj){
							xhrObj.setRequestHeader("Content-Type", "application/octet-stream");
							xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
						},
						type: "POST",
						processData: false,
						data: new Blob([fileContentArrayBuffer], { type: fileType })
					})
					.fail(function (err) { thisApp.notify("Oops! execution failed - Face ID"); NProgress.done(); })
					.done(function (data) {
						if(data.length > 0){
							var faceIds_dected = []
							for (var i = 0; i < data.length; i++) { faceIds_dected.push(data[i]["faceId"]); }

							$.ajax({
								url: "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/identify",
								beforeSend: function (xhrObj) {
									xhrObj.setRequestHeader("Content-Type", "application/json");
									xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
								},
								type: "POST",
								data: JSON.stringify({ "personGroupId": "asugfr", "faceIds": faceIds_dected, "maxNumOfCandidatesReturned": 5 })
							})
							.fail(function (err) { thisApp.notify("Oops! execution failed"); NProgress.done(); })
							.done(function (data) {
								// console.log(data);
								thisApp.flagPresent(data);
								NProgress.done();
							});
						}
						else{
							thisApp.notify("No Face IDs detected");
							NProgress.done();
						}
					});
				};
			};
			request.send();
		},
		onFail: function(message){ console.log(message); },
	},
	mounted: function(){
		console.log("<take-attendance-pic>: Init!");
	},
	watch: {}
});