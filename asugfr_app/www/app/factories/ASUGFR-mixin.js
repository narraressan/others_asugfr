


// global functions
Vue.mixin({
	methods: {
		subsKey_general: function(){ return "9dcb885048b74c12b82115e09f40bef8"; },
		notify: function(message){ notie.alert({ text: message, position: "bottom" }); },
		request: function(thisMethod, thisURL, thisData, success){
			var thisVue = this;
			NProgress.start();
			$.ajax({
				method: thisMethod,
				url: thisURL,
				contentType: "application/x-www-form-urlencoded",
				data: JSON.stringify(thisData)
			})
			.fail(function(response){
				thisVue.notify("Oops! execution failed");
				NProgress.done();
			})
			.done(function(data, textStatus, xhr){
				success(data);
				NProgress.done();
			});
		},
	}
})